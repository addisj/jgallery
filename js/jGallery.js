(function( $ ){

  $.fn.jGallery = function( options ) {
    
    // Create some defaults, extending them with any options that were provided
    var settings = $.extend( {
      'popupContainer'      : '#popup',
      'popupContainerWidth' : '0.8',
      'galleryContainer'    : '.gallery',
      'clientWidth'         : document.documentElement.clientWidth,
      'clientHeight'        : document.documentElement.clientHeight,
      'startPos'            : 25,
      'maxHeight'           : 0,
      'maxWidth'            : 0,
      'maxThumbHeight'      : 0,
      'maxThumbWidth'       : 0,
      'totalThumbHeight'    : 0,
      'totalThumbWidth'     : 0,
      'extraThumbHeight'    : 4,
      'extraThumbWidth'     : 4,
      'loadedImages'        : [],
      'gallerySize'         : 0,
      'percent'             : 0,
      'startPanel'          : 1,
      'startFadeIn'         : 3000,
      'thumbView'           : false,
      'thumbClicked'        : false
    }, options);
    
    for(var index in settings) {
      if(typeof options[index] == "undefined") options[index] = settings[index];
    }
    
    if($(settings["popupContainer"]+' .gallery').is('*')){}
    else {
      $(settings["popupContainer"]+' .scroller').wrap('<div class=\"thumbs\" />');
      $(settings["popupContainer"]+' .scroller img').wrap('<a class=\"roundBorder\" />');
      $(settings["popupContainer"]+' .thumbs').wrap('<div class=\"gallery\" />');
    }
    
    preloadImages(settings);
    
    onResize(settings);
        
    $.fn.hasParent = function(objs) {
      // ensure that objs is a jQuery array
      objs = $(objs); var found = false;
      $(this[0]).parents().andSelf().each(function() {
        if ($.inArray(this, objs) !== -1) {
          found = true;
          return false; // stops the each...
        }
      });
      return found;
    };
    
    function debouncer( func , timeout ) {
       var timeoutID , timeout = timeout || 200;
       return function () {
          var scope = this , args = arguments;
          clearTimeout( timeoutID );
          timeoutID = setTimeout( function () {
              func.apply( scope , Array.prototype.slice.call( args ) );
          } , timeout );
       }
    }
    
    function repeat(str, n) {
      return new Array( n + 1 ).join( str );
    }
    
    function setContainerWidth(options){
      var thumbs_margin_top = (options["maxThumbHeight"]/2)+10;
      options["containerWidth"] = $(options["popupContainer"]).eq(0).width();
      $(options["galleryContainer"]).css({"width":options["containerWidth"]-(options["startPos"]*2), "margin-top":"-"+thumbs_margin_top+"px"});
      
      return options;
    }
    
    function onResize(options){
      var clientWidth;
      if(clientWidth === undefined) clientWidth = options["clientWidth"];
      var clientHeight;
      if(clientHeight === undefined) clientHeight = options["clientHeight"];
    
      var func = function(){
        if((clientWidth !== document.documentElement.clientWidth) || (clientHeight !== document.documentElement.clientHeight)){
          options = setContainerWidth(options);
          
          var popMargTop = $(options["popupContainer"]).height() / 2;
          var popMargLeft = $(options["popupContainer"]).width() / 2;
          
          var width_offset = 0;
          if(clientWidth !== document.documentElement.clientWidth){
            if(clientWidth > document.documentElement.clientWidth){
              var sign="-";
              width_offset = Math.abs(((clientWidth - document.documentElement.clientWidth)/2)*(options["popupContainerWidth"]));
            } else {
              var sign="+";
              width_offset = Math.abs(((document.documentElement.clientWidth - clientWidth)/2)*(options["popupContainerWidth"]));
            }
          }
          var args={};
          args["left"]=sign+"="+width_offset+"px";
          $(options['popupContainer']+' .scroller').stop().animate(args, 'slow');
          
          $(options['popupContainer']).stop().animate({marginTop: -popMargTop, marginLeft: -popMargLeft});
        }
        options["clientWidth"] = clientWidth = document.documentElement.clientWidth;
        options["clientHeight"] = clientHeight = document.documentElement.clientHeight;
      };
      window.onresize = debouncer(func, 1000);
      
      return options;
    }
    function initGallery(options){
      var mh = options["maxHeight"];
      var mw = options["maxWidth"];
      var mth = options["maxThumbHeight"];
      var mtw = options["maxThumbWidth"];
      var th = options["totalThumbHeight"];
      var tw = options["totalThumbWidth"]+10+(options["gallerySize"]*options["extraThumbWidth"]);
      var startPos = options["startPos"];
      
      var scroller = options["scroller"] = $(options["popupContainer"]+' .scroller');
      
      if($(options["popupContainer"]+' .wrap').is('*')){}
      else {
        $(options["popupContainer"]+' .gallery').wrap('<div class=\"bg\" />');
        $(options["popupContainer"]+' .bg').prepend('<h1 />').wrap('<div class=\"wrap\" />');
        $(options["popupContainer"]+' .wrap').prepend('<a href=\"#\" class=\"close\"><img src=\"/resources/jGallery/images/icon_close.png\" class=\"btn_close\" title=\"Close Window\" alt=\"Close\" /></a><img class=\"border_left\" src=\"/resources/jGallery/images/border_left.png\" /><img class=\"border_right\" src=\"/resources/jGallery/images/border_right.png\" /><a class=\"left\"><img src=\"/resources/jGallery/images/arrow_left_blue.png\" /></a><a class=\"right\"><img src=\"/resources/jGallery/images/arrow_right_blue.png\" /></a>');
        $(options["popupContainer"]+' .wrap').append('<div class=\"large\"><a href=\"#\" class=\"close2\"><img src=\"/resources/jGallery/images/icon_close.png\" class=\"btn_close2\" title=\"Close Window\" alt=\"Close\" /></a><img /><div class=\"info\"><h2 /><p /><span class=\"count\"></span></div></div>');
        if($('#content h3').is('*')){
          $(options["popupContainer"]+' .bg h1').text($('#content h3').text());
        } else $(options["popupContainer"]+' .bg h1').text($('#content h2').text());
        
      }
      
      scroller.find('li img').css({width:'auto', visibility:'visible'});
      $(options["popupContainer"]+' h1').css({display:'block'});
      $(options["popupContainer"]+' .scroller a').removeClass('fadeOut');
      scroller.find('li a').append('<span />');
      
      scroller.find('a > span').each(function(){
        $(this).css({"width":$(this).prev('img').width()+"px"})/*.show()*/;
      });
      
      var large_image_div = options["large_image_div"] = $(options["popupContainer"]+' .large');
      
      options = setContainerWidth(options);
      var cw = options["containerWidth"];
      
      $(options["galleryContainer"]+' .loadingBar').hide();
      
      options["thumbView"] = true;
      large_image_div.hide();
      $(options["popupContainer"]+' a.left').hide();
      
      if(tw > cw){
        $('a.right').show();
        $(options["galleryContainer"]).css({"left":startPos+'px'});//"margin":'0 '+startPos+'px'
      }
      
      large_image_div
        .delegate('a.close2','click',function(){
          $(this).parents('.large').css({display:'none'});
          var index2 = Number($(this).parents('.large').find('img:not(.btn_close2)').attr('class'));
          var current_thumb2 = scroller.find('li:eq('+index2+')');
          var current_thumb2_width = current_thumb2.find('span').width();
          current_thumb2
            .find('a').addClass('roundBorder')
            .find('img')
              .stop().animate({width:current_thumb2_width}, 'fast', function(){})
              .css({/*width: current_thumb2_width, */visibility:'visible'});
          
          if(scroller.position().left > 0) scroller.stop().animate({left:'0px'}, 'slow');
          $(options["popupContainer"]+' h1').css({display:'block'});
          $(options["popupContainer"]+' .scroller a').removeClass('fadeOut');
          options["thumbView"] = true;
          options["thumbClicked"] = false;
        });
      
      scroller 
        .css({"width":tw*2,"left":"0px","visibility":"visible"})
        .delegate('a','click', function(){
          var imageLeftPos = $(this).offset().left;
          var indexGoTo = $(this).parents('li').index();
          
          /* Testing 2nd fade layer to overlay thumbnails when the large image is open
          $('body').append('<div id="fade2"></div>');
          $('#fade2').css({'filter' : 'alpha(opacity=80)'}).fadeIn();*/
          
          large_image_div.css({display:'block'});
          $(options["popupContainer"]+' .scroller a').addClass('fadeOut');
          $(options["popupContainer"]+' h1').css({display:'none'});
          options["thumbClicked"] = true;
          animateGallery(options, this, imageLeftPos, indexGoTo);
        });
      
      $(options["popupContainer"]+' a.left, '+options["popupContainer"]+' a.right').click(function(){
        animateGallery(options, this, null, null);
      });
    }
    
    function animateGallery(options, obj, imageLeftPos, indexGoTo){
      
      var scroller = options["scroller"];
      
      if(!scroller.is(':animated')){
        var large_image_div = options["large_image_div"];
        var original_thumb_width = options["original_thumb_width"];
        var scrollerLeftPos = scroller.position().left;
        var args={};
        
        //if no image is clicked (i.e. paging through images 1 by 1)
        if(options["thumbView"] && !options["thumbClicked"]){
          var direction = obj.className.replace(".","");
          (direction == "left") ? sign = "+" : sign = "-";
          
          if((direction == "right" && scrollerLeftPos > (options["totalThumbWidth"]-options["containerWidth"])*-1) || (direction == "left" && scrollerLeftPos < 0)) {
            //alert('3: '+imageOffset+' : '+galleryContainerMidpoint+' : '+direction+' : '+sign);
            args["left"]=sign+"="+(options["containerWidth"]-(options["startPos"]*2))+"px";
            scroller.stop().animate(args, 'slow');
          }
          
          if(direction == "right" && (scrollerLeftPos-(options["containerWidth"]-(options["startPos"]*2))) < (options["totalThumbWidth"]-options["containerWidth"])*-1) {
            $(options["popupContainer"]+' a.right').hide();
            $(options["popupContainer"]+' a.left').show();
          } else if(direction == "left" && (scrollerLeftPos+(options["containerWidth"]-(options["startPos"]*2)))>=0){
            $(options["popupContainer"]+' a.right').show();
            $(options["popupContainer"]+' a.left').hide();
          } else {
            $(options["popupContainer"]+' a.right').show();
            $(options["popupContainer"]+' a.left').show();
          }
          
        } else {
          
          var index = Number(large_image_div.find('img:not(.btn_close2)').attr('class'));
          if(isNaN(index)) index = 0;
          var current_thumb = scroller.find('li:eq('+index+')');
          
          if(imageLeftPos === null) {
            
            var direction = obj.className.replace(".","");
            if(direction == "left"){
              sign = "+";
              indexGoTo = index-1;
            } else {
              sign = "-";
              indexGoTo = index+1;
            }
            var goto_thumb = scroller.find('li:eq('+indexGoTo+')');
            imageLeftPos = goto_thumb.find('a').offset().left;
          } else {
            var goto_thumb = scroller.find('li:eq('+indexGoTo+')');
          }
          
          var galleryContainerMidpoint = $(options["galleryContainer"]).width()/2;
          var popupContainerLeftPos = $(options["popupContainer"]).offset().left;
          imageOffset = imageLeftPos-popupContainerLeftPos-options["startPos"];
            
          if(imageOffset > galleryContainerMidpoint){
            var direction = "right";
            var sign="-";
            var width_offset = imageOffset - galleryContainerMidpoint;
          } else {
            var direction = "left";
            var sign="+";
            var width_offset = galleryContainerMidpoint - imageOffset;
          }
          
          if(direction == 'right' && indexGoTo<=(options["gallerySize"]-1)){
            
            var current_thumb_width = current_thumb.find('span').width();
            
            var current_large_width = Number(current_thumb.find('img').get(0).getAttribute('width'));
            var goto_thumb_width = goto_thumb.find('img').width();
            var goto_large_width = Number(goto_thumb.find('img').get(0).getAttribute('width'));
            
            (original_thumb_width!==null) ? current_thumb_width = original_thumb_width : current_thumb_width = current_thumb_width;
    
            if(options["thumbView"]){
              args["left"]=sign+"="+(width_offset+(goto_large_width/2))+"px";
              options["thumbView"] = false;
            } else {
              args["left"]=sign+"="+(width_offset+(goto_large_width/2)+(current_thumb_width-current_large_width)+options["extraThumbWidth"])+"px";
            }
            
            large_image_div.css({'display':'none'});
            
            current_thumb
              .find('a').addClass('roundBorder')
              .find('img')
                .css({width: current_thumb_width, visibility:'visible'});
              
            goto_thumb
              .find('a').removeClass('roundBorder')
              .find('img')
                .css({width: goto_large_width, visibility:'hidden'})
                /*.stop().animate({
                  //opacity: 0.25,
                  width : goto_large_width*//*,
                  //  height: 300*//*
                  }, 'slow')*/; //use 'slow' for bubble effect
            
            large_image_div
              .find('img:not(.btn_close2)')
                .attr('src', goto_thumb.find('img').attr('src'))
                .attr('alt', goto_thumb.find('img').attr('alt'))
                .attr('class',indexGoTo)
                .end().find('span.count').html((indexGoTo+1)+' of '+options["gallerySize"]);
             
            scroller.stop().animate(args, 'slow');
            
            large_image_div.find('div.info > h2').html(goto_thumb.find('img').attr('alt'));
            large_image_div.find('div.info > p').html(goto_thumb.find('span').html());
            
            var width_offset = goto_thumb.find('img').get(0).getAttribute('width')/2;
            var height_offset = goto_thumb.find('img').get(0).getAttribute('height')/2+20;
            
            large_image_div.find('div.info').css({"width":width_offset*2});
            large_image_div.css({"margin-top":"-"+height_offset+"px", "margin-left":"-"+width_offset+"px"}).fadeIn('slow');
            
            original_thumb_width = options["original_thumb_width"] = null;
          } else if(direction == 'left' && indexGoTo>=0){
    
            large_image_div.css({'display':'none'});
            
            var current_thumb_width = current_thumb.find('span').width();
            
            var current_large_width = Number(current_thumb.find('img').get(0).getAttribute('width'));
            var goto_thumb_width = goto_thumb.find('img').width();
            var goto_large_width = Number(goto_thumb.find('img').get(0).getAttribute('width'));
    
            (original_thumb_width!==null) ? current_thumb_width = original_thumb_width : current_thumb_width = current_thumb_width;
            
            current_thumb
              .find('a').addClass('roundBorder')
              .find('img')
                .css({width: current_thumb_width, visibility:'visible'});
    
            args["left"]=sign+"="+(width_offset-(goto_large_width/2))+"px";
            
            scroller.stop().animate(args, 'slow');
            
            goto_thumb
              .find('a').removeClass('roundBorder')
              .find('img')
                .css({width: goto_large_width, visibility:'hidden'})
            
            large_image_div
              .find('img:not(.btn_close2)')
                .attr('src', goto_thumb.find('img').attr('src'))
                .attr('alt', goto_thumb.find('img').attr('alt'))
                .attr('class',indexGoTo)
                .end().find('span.count').html((indexGoTo+1)+' of '+options["gallerySize"]);
            
            large_image_div.find('div.info > h2').html(goto_thumb.find('img').attr('alt'));
            large_image_div.find('div.info > p').html(goto_thumb.find('span').html());
            
            var width_offset = goto_thumb.find('img').get(0).getAttribute('width')/2;
            var height_offset = goto_thumb.find('img').get(0).getAttribute('height')/2+20;
            
            large_image_div.find('div.info').css({"width":width_offset*2});
            large_image_div.css({"margin-top":"-"+height_offset+"px", "margin-left":"-"+width_offset+"px"}).fadeIn('slow');
            
            options["thumbView"] = false;
            original_thumb_width = options["original_thumb_width"] = null;
          }
          
          if(indexGoTo===(options["gallerySize"]-1)){
            $(options["popupContainer"]+' a.right').hide();
            $(options["popupContainer"]+' a.left').show();
          } else if(indexGoTo===0){
            $(options["popupContainer"]+' a.right').show();
            $(options["popupContainer"]+' a.left').hide();
          } else {
            $(options["popupContainer"]+' a.right').show();
            $(options["popupContainer"]+' a.left').show();
          }
          
        }
      
      }
    }
    
    // Get actual image dimensions during preloading 
    function getDimensions(el, options){
      options["loadedImages"].push(el);
      options["percent"] = Math.round((options["loadedImages"].length / options["gallerySize"])*100) + '%';
      $(options["galleryContainer"]+' .loadingBar .percent').html(options["percent"]);
      
      //get thumb height/width
      var elHeight = $(el).outerHeight()+options["extraThumbHeight"];
      var elWidth = $(el).outerWidth()+10+options["extraThumbWidth"];
      if(elHeight > options["maxThumbHeight"]){options["maxThumbHeight"] = elHeight;}
      if(elWidth > options["maxThumbWidth"]){options["maxThumbWidth"] = elWidth;}
      options["totalThumbHeight"]+=elHeight;
      options["totalThumbWidth"]+=elWidth;
      
      //get actual height/width
      //var large = $(el).clone();
      var w = $(el).attr('width');
      var h = $(el).attr('height');
      var w_css = $(el).css('width')+options["extraThumbWidth"];
      var h_css = $(el).css('height')+options["extraThumbHeight"];
      $(el).removeAttr("width").removeAttr("height").css({'width': '', 'height': ''});
      var largeImg =  new Image();
      largeImg.src = el.src;
      if(largeImg.height > options["maxHeight"]){options["maxHeight"] = largeImg.height;}
      if(largeImg.width > options["maxWidth"]){options["maxWidth"] = largeImg.width;}
      $(el).attr('width',largeImg.width).attr('height',largeImg.height).css({'width':w_css,'height':h_css});
      
      if (options["loadedImages"].length === options["gallerySize"]){
          initGallery(options);
      }
      return options;
    }
    
    function preloadImages(options) {
      var gallery = $(options["galleryContainer"]);
      //TODO: need to only add loadingBar on first load
      gallery.find('.thumbs').prepend('<div class="loadingBar"><p><strong>Loading <span class="percent"></span></strong>&nbsp;<img src="/calendar/images/classic-loader.gif" width="16" height="16" /></p></div>');
      options["gallerySize"]=gallery.find('.scroller img').size();
      gallery.find('.scroller img').each(function(i){
        if ($.inArray(i, options["loadedImages"]) < 0){ //don't double count images
          if(this.complete || this.readyState == 'complete'){
            getDimensions(this, options);
          } else {
            $(this).bind('load readystatechange', function(e){
                if(this.complete || (this.readyState == 'complete' && e.type == 'readystatechange')){
                  getDimensions(this, options);
                }
            });
            var src = this.src;
            this.src = '#';
            this.src = src;
          }
        }
      });
    };

  };
})( jQuery );

