jQuery.fn.hasParent = function(objs) {
  // ensure that objs is a jQuery array
  objs = jQuery(objs); var found = false;
  jQuery(this[0]).parents().andSelf().each(function() {
    if (jQuery.inArray(this, objs) !== -1) {
      found = true;
      return false; // stops the each...
    }
  });
  return found;
};
function debouncer( func , timeout ) {
   var timeoutID , timeout = timeout || 200;
   return function () {
      var scope = this , args = arguments;
      clearTimeout( timeoutID );
      timeoutID = setTimeout( function () {
          func.apply( scope , Array.prototype.slice.call( args ) );
      } , timeout );
   }
}
function repeat(str, n) {
  return new Array( n + 1 ).join( str );
}
function setContainerWidth(options){
  var thumbs_margin_top = (options["maxThumbHeight"]/2)+10;
	options["containerWidth"] = jQuery(options["popupContainer"]).eq(0).width();
  jQuery(options["galleryContainer"]).css({"width":options["containerWidth"]-(options["startPos"]*2), "margin-top":"-"+thumbs_margin_top+"px"});
  
	return options;
}
function onResize(options){
  var clientWidth;
  if(clientWidth === undefined) clientWidth = options["clientWidth"];
  var clientHeight;
  if(clientHeight === undefined) clientHeight = options["clientHeight"];
  //alert(clientWidth+' : '+clientHeight);
  var func = function(){
  	if((clientWidth !== document.documentElement.clientWidth) || (clientHeight !== document.documentElement.clientHeight)){
	    options = setContainerWidth(options);
	    
	    var popMargTop = jQuery(options["popupContainer"]).height() / 2;
	    var popMargLeft = jQuery(options["popupContainer"]).width() / 2;
	    
	    //alert(clientWidth+' : '+document.documentElement.clientWidth);
	    var width_offset = 0;
	    if(clientWidth !== document.documentElement.clientWidth){
  	    if(clientWidth > document.documentElement.clientWidth){
  	      
  	      var sign="-";
  	      
  	      //if(options["popupContainerWidth"].indexOf('px') != -1) {
  	      //  width_offset = Math.abs(((clientWidth - document.documentElement.clientWidth)/2)*(Number(options["popupContainerWidth"].replace('px',''))/document.documentElement.clientWidth));
  	      //} else {
  	        width_offset = Math.abs(((clientWidth - document.documentElement.clientWidth)/2)*(options["popupContainerWidth"]));
  	      //}
  	      //alert('greater:'+document.documentElement.clientWidth+' : '+clientWidth+' : '+width_offset);
  	    } else {
  	      var sign="+";
  	      //if(options["popupContainerWidth"].indexOf('px') != -1) {
  	      //  width_offset = Math.abs(((document.documentElement.clientWidth - clientWidth)/2)*(Number(options["popupContainerWidth"].replace('px',''))/document.documentElement.clientWidth));
  	      //} else {
  	        width_offset = Math.abs(((document.documentElement.clientWidth - clientWidth)/2)*(options["popupContainerWidth"]));
  	      //}
  	      //alert('less:'+document.documentElement.clientWidth+' : '+clientWidth+' : '+width_offset+' : '+options["popupContainerWidth"]);
  	    }
	    }
      var args={};
      args["left"]=sign+"="+width_offset+"px";
	    jQuery(options['popupContainer']+' .scroller').stop().animate(args, 'slow');
	    
	    jQuery(options['popupContainer']).stop().animate({marginTop: -popMargTop, marginLeft: -popMargLeft});
  	}
  	options["clientWidth"] = clientWidth = document.documentElement.clientWidth;
    options["clientHeight"] = clientHeight = document.documentElement.clientHeight;
  };
  window.onresize = debouncer(func, 1000);
  
  return options;
}
function initGallery(options){
  var mh = options["maxHeight"];
  var mw = options["maxWidth"];
  var mth = options["maxThumbHeight"];
  var mtw = options["maxThumbWidth"];
  var th = options["totalThumbHeight"];
  var tw = options["totalThumbWidth"]+10+(options["gallerySize"]*options["extraThumbWidth"]);
  var startPos = options["startPos"];
  
  var scroller = options["scroller"] = jQuery(options["popupContainer"]+' .scroller');
  
  if(jQuery(options["popupContainer"]+' .wrap').is('*')){}
  else {
    jQuery(options["popupContainer"]+' .gallery').wrap('<div class=\"bg\" />');
    jQuery(options["popupContainer"]+' .bg').prepend('<h1 />').wrap('<div class=\"wrap\" />');
    jQuery(options["popupContainer"]+' .wrap').prepend('<a href=\"#\" class=\"close\"><img src=\"/resources/jGallery/images/icon_close.png\" class=\"btn_close\" title=\"Close Window\" alt=\"Close\" /></a><img class=\"border_left\" src=\"/resources/jGallery/images/border_left.png\" /><img class=\"border_right\" src=\"/resources/jGallery/images/border_right.png\" /><a class=\"left\"><img src=\"/resources/jGallery/images/arrow_left_blue.png\" /></a><a class=\"right\"><img src=\"/resources/jGallery/images/arrow_right_blue.png\" /></a>');
    jQuery(options["popupContainer"]+' .wrap').append('<div class=\"large\"><a href=\"#\" class=\"close2\"><img src=\"/resources/jGallery/images/icon_close.png\" class=\"btn_close2\" title=\"Close Window\" alt=\"Close\" /></a><img /><div class=\"info\"><h2 /><p /><span class=\"count\"></span></div></div>');
    if(jQuery('#content h3').is('*')){
      jQuery(options["popupContainer"]+' .bg h1').text(jQuery('#content h3').text());
    } else jQuery(options["popupContainer"]+' .bg h1').text(jQuery('#content h2').text());
    
  }
  
  scroller.find('li img').css({width:'auto', visibility:'visible'});
  jQuery(options["popupContainer"]+' h1').css({display:'block'});
  jQuery(options["popupContainer"]+' .scroller a').removeClass('fadeOut');
  scroller.find('li a').append('<span />');
  
  scroller.find('a > span').each(function(){
  	jQuery(this).css({"width":jQuery(this).prev('img').width()+"px"})/*.show()*/;
  	//alert(jQuery(this).prev('img').width());
  });
  //jQuery(options["popupContainer"]+' .wrap').append('');
  
  var large_image_div = options["large_image_div"] = jQuery(options["popupContainer"]+' .large');
  
  options = setContainerWidth(options);
  var cw = options["containerWidth"];
  
  jQuery(options["galleryContainer"]+' .loadingBar').hide();
  
  /* Start with first thumbnail as large image
  var index = 0;
  large_image_div
    .find('img:not(.btn_close2)')
        .attr('src', scroller.find('li:eq('+index+') img').attr('src'))
        .attr('alt', scroller.find('li:eq('+index+') img').attr('alt'))
        .attr('class', index)
    .end()
    .find('span.count').html((index+1)+' of '+options["gallerySize"]);
  large_image_div
    .find('div.info > h2').html(scroller.find('li:eq(0) img').attr('alt'))
    .end()
    .find('div.info > p').html(scroller.find('li:eq(0) p').html());
  
  var original_thumb = options["original_thumb"] = scroller.find('li:eq(0)');
  var original_thumb_width = options["original_thumb_width"] = original_thumb.find('img').width();
  var original_large_width = options["original_large_width"] = original_thumb.find('img').get(0).getAttribute('width');
  var original_thumb_height = options["original_thumb_height"] = original_thumb.find('img').height();
  var original_large_height = options["original_large_height"] = original_thumb.find('img').get(0).getAttribute('height');

  original_thumb.find('img').css({visibility:'hidden'}).stop().animate({
          width : original_large_width
          }, 1000, function(){});
  
  large_image_div.find('div').css({"width":original_large_width});
  large_image_div.css({"margin-top":"-"+((original_large_height/2)+40)+"px", "margin-left":"-"+(original_large_width/2)+"px"}).fadeIn('slow');
  */
  options["thumbView"] = true;
  large_image_div.hide();
  jQuery(options["popupContainer"]+' a.left').hide();
  
  if(tw > cw){
    //jQuery(options["galleryContainer"]+' .thumbs a.left, '+options["galleryContainer"]+' .thumbs a.right').show();
    jQuery('a.right').show();
    //jQuery(options["galleryContainer"]+' .scroller').css({"left":startPos+'px'});//"margin":'0 '+startPos+'px'
    jQuery(options["galleryContainer"]).css({"left":startPos+'px'});//"margin":'0 '+startPos+'px'
  }
  //alert(current_thumb_width);
  //alert((((cw-startPos*2)/2)/*-(current_thumb_width/2)*/));
  
  large_image_div
    .delegate('a.close2','click',function(){
      jQuery(this).parents('.large').css({display:'none'});
      var index2 = Number(jQuery(this).parents('.large').find('img:not(.btn_close2)').attr('class'));
      var current_thumb2 = scroller.find('li:eq('+index2+')');
      var current_thumb2_width = current_thumb2.find('span').width();
      current_thumb2
        .find('a').addClass('roundBorder')
        .find('img')
          .stop().animate({width:current_thumb2_width}, 'fast', function(){})
          .css({/*width: current_thumb2_width, */visibility:'visible'});
      
      if(scroller.position().left > 0) scroller.stop().animate({left:'0px'}, 'slow');
      jQuery(options["popupContainer"]+' h1').css({display:'block'});
      jQuery(options["popupContainer"]+' .scroller a').removeClass('fadeOut');
      options["thumbView"] = true;
      options["thumbClicked"] = false;
    });
  
  scroller
    //.css({"width":tw,"left":"0px"})
    //initial leftPos: (((cw-startPos*2)/2)-(original_large_width/2)) 
    .css({"width":tw*2,"left":"0px","visibility":"visible"})
    .delegate('a','click', function(){
      var imageLeftPos = jQuery(this).offset().left;
      var indexGoTo = jQuery(this).parents('li').index();
      //alert('indexGoTo: '+indexGoTo);
      
      /* Testing 2nd fade layer to overlay thumbnails when the large image is open
      jQuery('body').append('<div id="fade2"></div>');
      jQuery('#fade2').css({'filter' : 'alpha(opacity=80)'}).fadeIn();*/
      
      large_image_div.css({display:'block'});
      jQuery(options["popupContainer"]+' .scroller a').addClass('fadeOut');
      jQuery(options["popupContainer"]+' h1').css({display:'none'});
      options["thumbClicked"] = true;
      animateGallery(options, this, imageLeftPos, indexGoTo);
      
      //////var args={};
      //////var img_large_width = jQuery(this).find('img').get(0).getAttribute('width');
      //////var img_large_height = jQuery(this).find('img').get(0).getAttribute('height');
      //alert(large_width);
      //////var container_half_width = cw/2;
      //////var img_half_width = img_large_width/2;
      //////var img_half_height = img_large_height/2;
      //////args["left"]="-="+(container_half_width)+"px";
      
      //////jQuery(this).parents('.wrap').find('.large img')
      //////  .attr('src',jQuery(this).find('img').attr('src').replace("_cropped", ""))
        /*.attr('width',jQuery(this).find('img').get(0).getAttribute('width'))
        .attr('height',jQuery(this).find('img').get(0).getAttribute('height'))*/;
      //jQuery(this).parent('.wrap').find('.large').stop().animate({
        //opacity: 0.25,
      //  left : args["left"]/*,
      //  height: 300*/
      //}, 1000, function(){});
      //////jQuery(this).parents('.wrap').find('.large').css({"margin-top":"-"+img_large_height+"px", "margin-left":"-"+img_large_width+"px"});
      //////jQuery(this).parents('.wrap').find('.large div').text(jQuery(this).find('img').attr('alt'));
      
      
      
      //jQuery(options["galleryContainer"]+' .scroller').stop().animate(args, 'slow', function() {});
      //jQuery(this).find('img').css({visibility:'hidden'}).stop().animate({
		    //opacity: 0.25,
		  //  width : img_large_width/*,
		  //  height: 300*/
		  //}, 1000, function(){});
    })
    /*.delegate('a', 'mouseenter', 
	    function(){
	    	//var margin_top = jQuery(this).css('margin-top');
	    	//(margin_top.indexOf('-10')!=-1) ? margin_top = '0px' : margin_top = '-10px';
				jQuery(this+' > p').show();
				jQuery(this).css({marginTop:"-10px"});
				//jQuery(this).stop().animate({marginTop:"-10px"}, 200, function(){}).css({marginTop:"-10px"})
				;
	  	}
    )
    .delegate('a', 'mouseleave', 
      function(){
        jQuery(this+' > p').hide();   
        jQuery(this).stop().animate({marginTop:"0px"}, 'slow')
          //.css({marginTop:"0px"})
           ;        
      }
    )*/;
  
  jQuery(options["popupContainer"]+' a.left, '+options["popupContainer"]+' a.right').click(function(){
	  animateGallery(options, this, null, null);
  });
  //jQuery(options["galleryContainer"]+' .scroller img').eq(0).click();
}

function animateGallery(options, obj, imageLeftPos, indexGoTo){
  
  var scroller = options["scroller"];
  
  if(!scroller.is(':animated')){
    var large_image_div = options["large_image_div"];
    var original_thumb_width = options["original_thumb_width"];
    var scrollerLeftPos = scroller.position().left;
    var args={};
    
    //if no image is clicked (i.e. paging through images 1 by 1)
    if(options["thumbView"] && !options["thumbClicked"]){
      var direction = obj.className.replace(".","");
      (direction == "left") ? sign = "+" : sign = "-";
      
      //if((direction == "right" && scrollerLeftPos > ((options["totalThumbWidth"]+(options["gallerySize"]*options["extraThumbWidth"]))-options["containerWidth"])*-1) || (direction == "left" && scrollerLeftPos < 0)) {
      if((direction == "right" && scrollerLeftPos > (options["totalThumbWidth"]-options["containerWidth"])*-1) || (direction == "left" && scrollerLeftPos < 0)) {
        //alert('3: '+imageOffset+' : '+galleryContainerMidpoint+' : '+direction+' : '+sign);
        args["left"]=sign+"="+(options["containerWidth"]-(options["startPos"]*2))+"px";
        scroller.stop().animate(args, 'slow');
      }
      
      //if(direction == "right" && (scrollerLeftPos-(options["containerWidth"]-(options["startPos"]*2))) < ((options["totalThumbWidth"]+(options["gallerySize"]*options["extraThumbWidth"]))-options["containerWidth"])*-1) {
      if(direction == "right" && (scrollerLeftPos-(options["containerWidth"]-(options["startPos"]*2))) < (options["totalThumbWidth"]-options["containerWidth"])*-1) {
        //if(direction == "right" && (leftPos-(cw-startPos)) < (tw-cw)*-1) {
        jQuery(options["popupContainer"]+' a.right').hide();
        jQuery(options["popupContainer"]+' a.left').show();
      } else if(direction == "left" && (scrollerLeftPos+(options["containerWidth"]-(options["startPos"]*2)))>=0){
        //alert('1: '+scrollerLeftPos);
        jQuery(options["popupContainer"]+' a.right').show();
        jQuery(options["popupContainer"]+' a.left').hide();
      } else {
        //alert('2: '+scrollerLeftPos);
        jQuery(options["popupContainer"]+' a.right').show();
        jQuery(options["popupContainer"]+' a.left').show();
      }
      
    } else {
      
      var index = Number(large_image_div.find('img:not(.btn_close2)').attr('class'));
      if(isNaN(index)) index = 0;
      //alert('index: '+index+' : '+large_image_div.find('img:not(.btn_close2)').size());
      var current_thumb = scroller.find('li:eq('+index+')');
      
      if(imageLeftPos === null) {
        //var isClicked = false; 
        
        var direction = obj.className.replace(".","");
        if(direction == "left"){
          sign = "+";
          indexGoTo = index-1;
        } else {
          sign = "-";
          indexGoTo = index+1;
        }
        var goto_thumb = scroller.find('li:eq('+indexGoTo+')');
        imageLeftPos = goto_thumb.find('a').offset().left;
      } else {
        //var isClicked = true;
        var goto_thumb = scroller.find('li:eq('+indexGoTo+')');
      }
      
      //var midpoint = (document.documentElement.clientWidth/2)*(options["popupContainerWidth"]/100);
      var galleryContainerMidpoint = jQuery(options["galleryContainer"]).width()/2;
      //var popupContainerLeftPos = (document.documentElement.clientWidth-jQuery(options["popupContainer"]))/2;
      var popupContainerLeftPos = jQuery(options["popupContainer"]).offset().left;
      imageOffset = imageLeftPos-popupContainerLeftPos-options["startPos"];
      //alert(galleryContainerMidpoint+' : '+imageLeftPos);
      //alert(direction);
        
      if(imageOffset > galleryContainerMidpoint){
        //alert(imageLeftPos+' : '+midpoint);
        var direction = "right";
        var sign="-";
        var width_offset = imageOffset - galleryContainerMidpoint;
        //alert('1: '+imageOffset+' : '+galleryContainerMidpoint+' : '+direction+' : '+sign);
        //alert('greater:'+document.documentElement.clientWidth+' : '+clientWidth+' : '+width_offset);
        //alert('1: '+width_offset+' : '+direction+' : '+sign);
      } else {
        //alert(imageLeftPos+' : '+midpoint);
        var direction = "left";
        var sign="+";
        var width_offset = galleryContainerMidpoint - imageOffset;
        //alert('2: '+imageOffset+' : '+galleryContainerMidpoint+' : '+direction+' : '+sign);
        //alert('less:'+document.documentElement.clientWidth+' : '+clientWidth+' : '+width_offset);
        //alert('2: '+width_offset+' : '+direction+' : '+sign);
      }
      //alert(direction);
    
    //alert(imageLeftPos);
    
    
    //alert(width_offset);
    /*if(hasLeftPos){
      
    } else {
      
      var direction = obj.className.replace(".","");
      (direction == "left") ? sign="+" : sign="-";
      var width_offset = 0;
      
    }*/
    
    
    //args["left"]=sign+"="+(cw-(startPos*2))+"px";
    //OLD:args["left"]=sign+"="+(cw-startPos)+"px";
    
    
      //alert(Number(large_image_div.find('img').attr('class')));
      
      
      //alert(direction+' : '+sign+' : '+width_offset+' : '+index);
      //var index = index = Number(large_image_div.find('img').attr('class'));
      //alert('3: '+width_offset+' : '+direction+' : '+sign);
      
      if(direction == 'right' && indexGoTo<=(options["gallerySize"]-1)){
        //alert('1: '+indexGoTo+' : '+direction);
        
        //if(indexGoTo === null) indexGoTo = index+1;
        //alert('here1: '+(index+2));
        //alert(options["clientWidth"]);
        
        
        
        
        
        //var next_thumb = scroller.find('li:eq('+(indexGoTo)+')');
        
        var current_thumb_width = current_thumb.find('span').width();
        
        var current_large_width = Number(current_thumb.find('img').get(0).getAttribute('width'));
        var goto_thumb_width = goto_thumb.find('img').width();
        var goto_large_width = Number(goto_thumb.find('img').get(0).getAttribute('width'));
        //alert(current_thumb_width+' : '+current_large_width+' : '+goto_thumb_width+' : '+goto_large_width);
        //var clone_first = jQuery(options["popupContainer"]+' .scroller li').eq(0);
        
        (original_thumb_width!==null) ? current_thumb_width = original_thumb_width : current_thumb_width = current_thumb_width;
        //alert(large_image_div.css('display'));
        if(options["thumbView"]){
          //alert('thumbView');
          args["left"]=sign+"="+(width_offset+(goto_large_width/2))+"px";
          options["thumbView"] = false;
        } else {
          //alert('notthumbView');
          args["left"]=sign+"="+(width_offset+(goto_large_width/2)+(current_thumb_width-current_large_width)+options["extraThumbWidth"])+"px";
        }
        
        large_image_div.css({'display':'none'});
        
        current_thumb
          .find('a').addClass('roundBorder')
          .find('img')
            .css({width: current_thumb_width, visibility:'visible'});
          ////.stop().animate({
            //opacity: 0.25,
          ////  width : current_thumb_width/*,
            //  height: 300*/
          ////  }, 500, function(){}).css({visibility:'visible'});
        
        //args["left"]=sign+"="+(current_thumb_width+10)+"px";
        //args["left"]=sign+"="+((current_thumb_width/2)+10)+"px";
        //args["left"]=sign+"="+((next_large_width/2)-((current_large_width/2)-(current_thumb_width/2)))+"px";
        //alert(current_thumb_width+' : '+(next_large_width/2)+' : '+(current_large_width/2));
        //args["left"]=sign+"="+((width_offset+current_thumb_width+(next_large_width/2)+10)-(current_large_width/2))+"px";
        //args["left"]=sign+"="+(width_offset+((goto_large_width/2)-(current_large_width/2)))+"px";

        //TODO: Change animation so it doesn't bubble, but slides like the left direction
        goto_thumb
          .find('a').removeClass('roundBorder')
          .find('img')
            .css({width: goto_large_width, visibility:'hidden'})
            /*.stop().animate({
              //opacity: 0.25,
              width : goto_large_width*//*,
              //  height: 300*//*
              }, 'slow')*/; //use 'slow' for bubble effect
        
        large_image_div
          .find('img:not(.btn_close2)')
            .attr('src', goto_thumb.find('img').attr('src'))
            .attr('alt', goto_thumb.find('img').attr('alt'))
            .attr('class',indexGoTo)
            .end().find('span.count').html((indexGoTo+1)+' of '+options["gallerySize"]);
         
        scroller.stop().animate(args, 'slow');
        
        large_image_div.find('div.info > h2').html(goto_thumb.find('img').attr('alt'));
        large_image_div.find('div.info > p').html(goto_thumb.find('span').html());
        
        var width_offset = goto_thumb.find('img').get(0).getAttribute('width')/2;
        var height_offset = goto_thumb.find('img').get(0).getAttribute('height')/2+20;
        
        large_image_div.find('div.info').css({"width":width_offset*2});
        large_image_div.css({"margin-top":"-"+height_offset+"px", "margin-left":"-"+width_offset+"px"}).fadeIn('slow');
        
        original_thumb_width = options["original_thumb_width"] = null;
      } else if(direction == 'left' && indexGoTo>=0){
        //alert('2: '+indexGoTo+' : '+direction);
        
        //if(indexGoTo === null) indexGoTo = index-1;
        //alert('here2: '+index);
        large_image_div.css({'display':'none'});
        
        //var prev_thumb = scroller.find('li:eq('+(indexGoTo)+')');
        
        var current_thumb_width = current_thumb.find('span').width();
        
        var current_large_width = Number(current_thumb.find('img').get(0).getAttribute('width'));
        var goto_thumb_width = goto_thumb.find('img').width();
        var goto_large_width = Number(goto_thumb.find('img').get(0).getAttribute('width'));
        //alert(current_thumb_width+' : '+current_large_width+' : '+goto_thumb_width+' : '+goto_large_width);
        //var clone_last = jQuery(options["popupContainer"]+' .scroller li').eq(options["gallerySize"]-1);
        
        (original_thumb_width!==null) ? current_thumb_width = original_thumb_width : current_thumb_width = current_thumb_width;
        
        current_thumb
          .find('a').addClass('roundBorder')
          .find('img')
            .css({width: current_thumb_width, visibility:'visible'});
            ////.stop().animate({
              //opacity: 0.25,
              ////width : current_thumb_width/*,
              //  height: 300*/
             ////}, 'slow', function(){}).css({visibility:'visible'});
        
        //args["left"]=sign+"="+(prev_thumb_width+10)+"px";
        //args["left"]=sign+"="+((current_thumb_width/2)+10)+"px";
        //args["left"]=sign+"="+((current_large_width/2)-(current_thumb_width/2)+(prev_thumb_width/2)+10)+"px";
        //args["left"]=sign+"="+((width_offset+prev_thumb_width+(current_large_width/2)+10)-(prev_large_width/2))+"px";
        //args["left"]=sign+"="+(width_offset+((current_large_width/2)-(goto_large_width/2)))+"px";
        //args["left"]=sign+"="+(width_offset+(current_large_width/2)+(goto_thumb_width-goto_large_width))+"px";
        //args["left"]=sign+"="+(width_offset+(goto_large_width/2)+(current_thumb_width-current_large_width))+"px";
        
        args["left"]=sign+"="+(width_offset-(goto_large_width/2))+"px";
        
        //alert('left');
        scroller.stop().animate(args, 'slow');
        
        goto_thumb
          .find('a').removeClass('roundBorder')
          .find('img')
            .css({width: goto_large_width, visibility:'hidden'})
            /*.stop().animate({
              //opacity: 0.25,
              width : goto_large_width*//*,
              //  height: 300*//*
              }, 'slow')*/;
        
        large_image_div
          .find('img:not(.btn_close2)')
            .attr('src', goto_thumb.find('img').attr('src'))
            .attr('alt', goto_thumb.find('img').attr('alt'))
            .attr('class',indexGoTo)
            .end().find('span.count').html((indexGoTo+1)+' of '+options["gallerySize"]);
        
        large_image_div.find('div.info > h2').html(goto_thumb.find('img').attr('alt'));
        large_image_div.find('div.info > p').html(goto_thumb.find('span').html());
        
        var width_offset = goto_thumb.find('img').get(0).getAttribute('width')/2;
        var height_offset = goto_thumb.find('img').get(0).getAttribute('height')/2+20;
        
        large_image_div.find('div.info').css({"width":width_offset*2});
        large_image_div.css({"margin-top":"-"+height_offset+"px", "margin-left":"-"+width_offset+"px"}).fadeIn('slow');
        
        options["thumbView"] = false;
        original_thumb_width = options["original_thumb_width"] = null;
      }
      
      if(indexGoTo===(options["gallerySize"]-1)){
        jQuery(options["popupContainer"]+' a.right').hide();
        jQuery(options["popupContainer"]+' a.left').show();
      } else if(indexGoTo===0){
        jQuery(options["popupContainer"]+' a.right').show();
        jQuery(options["popupContainer"]+' a.left').hide();
      } else {
        jQuery(options["popupContainer"]+' a.right').show();
        jQuery(options["popupContainer"]+' a.left').show();
      }
      
    //}
    }
  
  }
}

function getDimensions(el, options){
  options["loadedImages"].push(el);
  options["percent"] = Math.round((options["loadedImages"].length / options["gallerySize"])*100) + '%';
  jQuery(options["galleryContainer"]+' .loadingBar .percent').html(options["percent"]);
  
  //get thumb height/width
  var elHeight = jQuery(el).outerHeight()+options["extraThumbHeight"];
  var elWidth = jQuery(el).outerWidth()+10+options["extraThumbWidth"];
  if(elHeight > options["maxThumbHeight"]){options["maxThumbHeight"] = elHeight;}
  if(elWidth > options["maxThumbWidth"]){options["maxThumbWidth"] = elWidth;}
  options["totalThumbHeight"]+=elHeight;
  options["totalThumbWidth"]+=elWidth;
  
  //get actual height/width
  //var large = jQuery(el).clone();
  //jQuery('#footer').append('<p>1:'+large+'</p>');
  var w = jQuery(el).attr('width');
  var h = jQuery(el).attr('height');
  //alert('w:'+w+' h:'+h);
  var w_css = jQuery(el).css('width')+options["extraThumbWidth"];
  var h_css = jQuery(el).css('height')+options["extraThumbHeight"];
  jQuery(el).removeAttr("width").removeAttr("height").css({'width': '', 'height': ''});
  var largeImg =  new Image();
  largeImg.src = el.src;
  //largeImg.src = el.src.replace('_t.jpg','.jpg');
  //jQuery('#footer').append('<p>11:'+largeImg.width+'</p>');
  if(largeImg.height > options["maxHeight"]){options["maxHeight"] = largeImg.height;}
  if(largeImg.width > options["maxWidth"]){options["maxWidth"] = largeImg.width;}
  //alert('w:'+largeImg.width+' h:'+largeImg.height);
  jQuery(el).attr('width',largeImg.width).attr('height',largeImg.height).css({'width':w_css,'height':h_css});
  
  if (options["loadedImages"].length === options["gallerySize"]){
      initGallery(options);
  }
  return options;
}

function preloadImages(options) {
  var gallery = jQuery(options["galleryContainer"]);
  //TODO: need to only add loadingBar on first load
  gallery.find('.thumbs').prepend('<div class="loadingBar"><p><strong>Loading <span class="percent"></span></strong>&nbsp;<img src="/calendar/images/classic-loader.gif" width="16" height="16" /></p></div>');
  options["gallerySize"]=gallery.find('.scroller img').size();
  gallery.find('.scroller img').each(function(i){
    if (jQuery.inArray(i, options["loadedImages"]) < 0){ //don't double count images
      if(this.complete || this.readyState == 'complete'){
        getDimensions(this, options);
      } else {
        jQuery(this).bind('load readystatechange', function(e){
            if(this.complete || (this.readyState == 'complete' && e.type == 'readystatechange')){
              getDimensions(this, options);
            }
        });
        var src = this.src;
        this.src = '#';
        this.src = src;
      }
    }
  });
};

function jGallery(options){
	var default_options = {
	  'popupContainer'      : '#popup',
	  'popupContainerWidth' : '0.8',
		'galleryContainer'    : '.gallery',
		'clientWidth'         : document.documentElement.clientWidth,
		'clientHeight'        : document.documentElement.clientHeight,
		'startPos'            : 25,
		'maxHeight'           : 0,
		'maxWidth'            : 0,
		'maxThumbHeight'      : 0,
		'maxThumbWidth'       : 0,
		'totalThumbHeight'    : 0,
		'totalThumbWidth'     : 0,
		'extraThumbHeight'    : 4,
		'extraThumbWidth'    	: 4,
		'loadedImages'        : [],
		'gallerySize'         : 0,
		'percent'             : 0,
		'startPanel'          : 1,
		'startFadeIn'         : 3000,
		'thumbView'           : false,
		'thumbClicked'        : false
	};
	for(var index in default_options) {
		if(typeof options[index] == "undefined") options[index] = default_options[index];
	}
	
	if(jQuery(options["popupContainer"]+' .gallery').is('*')){}
	else {
  	jQuery(options["popupContainer"]+' .scroller').wrap('<div class=\"thumbs\" />');
  	jQuery(options["popupContainer"]+' .scroller img').wrap('<a class=\"roundBorder\" />');
  	jQuery(options["popupContainer"]+' .thumbs').wrap('<div class=\"gallery\" />');
	}
	
	preloadImages(options);
	
	onResize(options);
	
}