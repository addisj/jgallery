//When you click on a link with class of poplight and the href starts with a # 
$('a.poplight[href^=#], a.poplight2[href^=#]').click(function() {														  	
    var popID = $(this).attr('rel'); //Get Popup Name
    var popURL = $(this).attr('href'); //Get Popup href to define size

    //Pull Query & Variables from href URL
    var query= popURL.split('?');
    var dim= query[1].split('&');
    var popWidth = dim[0].split('=')[1]; //Gets the first query string value
    var popHeight = dim[1].split('=')[1]; //Gets the second query string value
    
    if(popWidth==='auto'){
      var popWidthFull = $('#' + popID).width()+'px';
    } else if(popWidth < 100){
      var popWidthFull = popWidth+'%';
    } else {
      var popWidthFull = popWidth+'px';
    }
    if(popHeight==='auto'){
    } else if(popHeight < 100){
      var popHeightFull = popHeight+'%';
    } else {
      var popHeightFull = popHeight+'px';
    }
    //Fade in the Popup and add close button
    //$('#' + popID).fadeIn().css({ 'width': Number( popWidth ) }).prepend('<a href="#" class="close"><img src="close_pop.png" class="btn_close" title="Close Window" alt="Close" /></a>');
    var args = {};
    args["width"] = popWidthFull;
    if(popHeight!=='auto'){
      args["height"] = popHeightFull;
    }
    $('#' + popID).fadeIn().css(args)/*.prepend('<a href="#" class="close"><img src="close_pop.png" class="btn_close" title="Close Window" alt="Close" /></a>')*/;
    
    //Define margin for center alignment (vertical   horizontal) - if there are paddings/borders in the css make sure to add the total to the height/width below before dividing by 2
    //alert($('#' + popID).height());
    var popMargTop = $('#' + popID).outerHeight() / 2;
    var popMargLeft = $('#' + popID).outerWidth() / 2;
    //alert($('#' + popID).height());
    //if(popID.indexOf('_video')>0){
    //	popMargTop = popMargTop + 100;
    //}
    //alert(popMargTop);
    //Apply Margin to Popup
    $('#' + popID).css({
        'margin-top' : -popMargTop,
        'margin-left' : -popMargLeft
    });

    //Fade in Background
    $('body').append('<div id="fade"></div>'); //Add the fade layer to bottom of the body tag.
    $('#fade').css({'filter' : 'alpha(opacity=80)'}).fadeIn(); //Fade in the fade layer - .css({'filter' : 'alpha(opacity=80)'}) is used to fix the IE Bug on fading transparencies 
    
    if(popID.indexOf('_video')>0) {
      videoGallery();
    } else if (popID.indexOf('_interactive')>0) {
    		// do what?
    } else if (popID.indexOf('_flash')>0) {
		// do what?
    } else {
      $('#'+popID).jGallery({'popupContainer':'#'+popID, 'popupContainerWidth':popWidth/100, 'galleryContainer':'.gallery','startPanel':1, 'startPos':25});
    }

    return false;
});

//Close Popups and Fade Layer
$('a.close, #fade').live('click', function() { //When clicking on the close or fade layer...
    $('#fade , div.popup_block').fadeOut(function() {
        $('#fade').remove();  //fade them both out
    });
		//$('article.mediaColumnSlideshow').css('position','relative');
    return false;
});